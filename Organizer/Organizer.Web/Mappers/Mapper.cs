﻿using Organizer.Repozitory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using Organizer.Web.ViewModels;

namespace Organizer.Web.Mappers
{
    public class Mapper
    {
        public static User ToRegisterViewModel(AccountViewModel model)
        {

            User user = new User();
            Category category = new Category();
            SHA1 sha1ob = SHA1.Create();
            Skill skill = new Skill();

            if (model.CategoryIdLevel1 == 1)
            {
                category.CategoryId = 1;
                
            }
            else
            {
                category.CategoryId = Convert.ToInt32(model.CategoryIdLevel2);
                user.Experience = model.Experience;
                user.Picture_Link = model.Picture_Link;
                user.Skills = model.Skills;
            }

            user.Name = model.Name;
            user.Surname = model.Surname;
            user.UserName = model.UserName;
            user.Password = Helper.Helper.GetMd5Hash(sha1ob, model.Password);
            user.Email = model.Email;
            user.CategoryId = category.CategoryId;
            user.Phone = model.Phone;

            return user;

        }
    }
}