﻿using Organizer.Repozitory.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Organizer.Web.ViewModels
{
    public class AccountViewModel // RegisterViewModel
    {

        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "SurName is required.")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [RegularExpression(@"^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$", ErrorMessage = "Please enter valid email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Username is required.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string ConfirmPassword { get; set; }


        [Required]
        public string Phone { get; set; }

        public string Experience { get; set; }

        [Display(Name="Photo")]
        public string Picture_Link { get; set; }



        public List<SelectListItem> MenuLevel1 { get; set; }
        public List<SelectListItem> MenuLevel2 { get; set; }
        public int? CategoryIdLevel1 { get; set; }
        public string CategoryIdLevel2 { get; set; }


        public List<SelectListItem> MenuLevel3 { get; set; }
        public string Skills { get; set; }
    }

}