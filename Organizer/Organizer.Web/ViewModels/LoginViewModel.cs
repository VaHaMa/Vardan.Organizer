﻿using Organizer.Repozitory.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace Organizer.Web.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "UserName is required")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }


        [Display(Name = "Remember me?")]
        public bool remember { get; set; }



        public bool login(string username, string password)
        {
            OrganizerEntities db = new OrganizerEntities();
            var user = db.Users.FirstOrDefault(model => model.UserName == username);
            if (user == null)
            {
                return false;
            }
            else
            {

                SHA1 sha1Hash = SHA1.Create();
                bool isLogin = Helper.Helper.VerifyMd5Hash(sha1Hash, password, user.Password);
                if (isLogin)
                {
                    HttpContext.Current.Session["CategoryId"] = user.CategoryId;
                    HttpContext.Current.Session["userid"] = user.UserId;
                    HttpContext.Current.Session["fullname"] = user.FullName;

                }
                return isLogin;
            }
        }

    }
}