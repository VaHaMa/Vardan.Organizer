﻿using Organizer.Repozitory.Entities;
using Organizer.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Organizer.Web.Controllers
{
    public class UserController : BaseController
    {

        public ActionResult Index(int? id)
        {
            return RedirectToAction("List", id);
        }

        public ActionResult List(int? id)
        {
            ViewBag.id = Convert.ToInt32(id);
            return View(id);
        }


        public ActionResult _List(int? id, int dropId = 0)
        {
            var request = repository.SelectAll<Request>().ToList();
            IEnumerable<User> list = null;

            if (id != null && id != 0)
            {
                list = repository.SelectAll<User>().Where(p => p.CategoryId == id).ToList();
            }
            else
            {
                list = repository.SelectAll<User>().Where(p=>p.CategoryId!=1).ToList();
            }

            if (dropId == 1)
            {
                int rateAverage;
                int count;
                foreach (var user in list)
                {
                    rateAverage = 0;
                    count = 0;
                    foreach (var req in request)
                    {
                        if (user.UserId == req.UserId)
                        {
                            count++;
                            rateAverage += req.Rate;
                        }
                    }
                    if (count != 0)
                    {
                        rateAverage /= count;
                        user.Rate = rateAverage;
                    }
                }

                list = list.OrderByDescending(p => p.Rate);
            }

            else if (dropId == 2)
            {
                foreach (var user in list)
                {
                    foreach (var req in request)
                    {
                        if (user.UserId == req.UserId)
                        {
                            user.Popular = Convert.ToInt32(req.Popular);
                        }

                    }
                }
                list = list.OrderByDescending(p => p.Popular);
            }


            return PartialView("_PartialList", list);
        }



        [MyAuth]
        public ActionResult Details(int id)
        {
            var user = repository.Get<User>(id);

            return View(user);

        }

        [MyAuth]
        [ActionName("Request")]
        public ActionResult RequestMeeting(int? id)
        {
            Request request = new Request();
            if (id != null)
            {
                request.UserId = Convert.ToInt32(id);
                request.ClientUserId = Convert.ToInt32(Session["userid"]);
            }

            return View(request);
        }

        [MyAuth]
        [ActionName("Request")]
        [HttpPost]
        public JsonResult RequestMeeting(Request request)
        {
            request.Popular = repository.SelectAll<Request>().Where(p => p.UserId == request.UserId).ToList().Count;

            Random rnd = new Random();
            request.Rate = rnd.Next(1, 10);
            request.Popular++;
            request.Accepted = 0;



            if (ModelState.IsValid)
            {
                repository.Insert(request);
                repository.Save();
            }
            return Json(request);
        }
    }
}