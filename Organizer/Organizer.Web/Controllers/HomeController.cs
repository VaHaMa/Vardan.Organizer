﻿using Organizer.Repozitory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Organizer.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LeftSideBar()
        {
            var cats = repository.SelectAll<Category>().Where(p => p.CategoryId != 1).ToList();
            return PartialView(cats);

        }

        [HttpPost]
        public JsonResult AutocompleteSearch(string term)
        {
            string l = term.ToUpper();
            var models = repository.SelectAll<User>().Where(a => a.FullName.ToUpper().Contains(l))
                            .Select(a => new { value = a.FullName }).Distinct(); 

            return Json(models, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search(string search)
        {
           string k= search.ToUpper();
           var list=repository.SelectAll<User>().Where(p => p.FullName.ToUpper().Contains(k)).ToList();

            return View("~/Views/User/_PartialList.cshtml", list) ;
        }

    }
}