﻿using Organizer.Repozitory.Entities;
using Organizer.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Organizer.Web.Controllers
{
    public class SpecialistController : BaseController
    {

        // GET: Specialist
        [MySpecAuth]
        public ActionResult Index(int id)
        {

            var list = repository.SelectAll<Request>().Where(p => p.UserId == id && p.Accepted == 0).ToList(); 
            return View(list);
        }

        [MySpecAuth]
        public ActionResult Details(int id)
        {
            var request = repository.Get<Request>(id);

            return View(request);

        }

        [MySpecAuth]
        public ActionResult Today()
        {
            var todayrRequest = repository.SelectAll<Request>()
                .Where(p => p.Accepted==1 && p.Date_Time==DateTime.Today);

            ViewBag.todayId = repository.SelectAll<Request>()
                .Select(p => p.UserId);
            return View(todayrRequest);

        }

        [HttpPost]
        [MySpecAuth]
        public ActionResult Status(Request request)
        {
            var requestUpd = repository.Get<Request>(request.RequestId);

            requestUpd.Accepted = request.Accepted;
            requestUpd.Reject_Description = request.Reject_Description;
            requestUpd.Date_Time = DateTime.Today;



            repository.Update(requestUpd);

            repository.Save();


            return RedirectToAction("Index", "Specialist", new { id = requestUpd.UserId });
        }
    }
}