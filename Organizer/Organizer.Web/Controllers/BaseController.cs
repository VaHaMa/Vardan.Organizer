﻿using Organizer.Repozitory.Entities;
using Organizer.Repozitory.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Organizer.Web.Controllers
{
    public class BaseController : Controller
    {
        public OrganizerEntities organizerEntities = new OrganizerEntities();

        protected IOrganizerRepository repository;
        public BaseController()
        {
            repository = new OrganizerRepository();
        }
        
    }
}