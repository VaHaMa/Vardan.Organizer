﻿ using Organizer.Repozitory.Entities;
using Organizer.Web.Filters;
using Organizer.Web.Mappers;
using Organizer.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Organizer.Web.Controllers
{
    public class AccountController : BaseController
    {
       
        public ActionResult Login()
        {
            LoginViewModel account = checkCookie();
            if (account == null)
                return View();

            else
            {
                LoginViewModel  loginModel = new LoginViewModel();
                if (loginModel.login(account.Username, account.Password))
                {

                    Session["username"] = account.Username;

                    int categoryId = Convert.ToInt32(Session["CategoryId"]);
                    int userId = Convert.ToInt32(Session["userid"]);
                    if (categoryId == 1)
                    {
                        return RedirectToAction("Index", "User");
                    }
                    else
                        return RedirectToAction("Index", "Specialist", new { id = userId });
                   

                }
                else
                {
                    ViewBag.Error = "Account's Invalid";
                    return View();
                }

            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {

            if (string.IsNullOrEmpty(model.Username) || string.IsNullOrEmpty(model.Password))
            {
                return RedirectToAction("Login");
            }

            if (model.login(model.Username, model.Password))
            {

                Session["username"] = model.Username;

                if (model.remember)
                {
                    HttpCookie ckUsername = new HttpCookie("username");
                    ckUsername.Expires = DateTime.Now.AddHours(1);
                    ckUsername.Value = model.Username;
                    Response.Cookies.Add(ckUsername);

                    HttpCookie ckPassword = new HttpCookie("password");
                    ckPassword.Expires = DateTime.Now.AddHours(1);
                    ckPassword.Value = model.Password;
                    Response.Cookies.Add(ckPassword);
                }
                int categoryId = Convert.ToInt32(HttpContext.Session["CategoryId"]);
                int userId = Convert.ToInt32(Session["userid"]);
                if (categoryId == 1)
                {
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    
                    return RedirectToAction("Index", "Specialist", new { id = userId });
                }


            }
            else
            {
                ViewBag.Error = "Account's Invalid";
                return View();
            }
        }

        public LoginViewModel checkCookie()
        {

            LoginViewModel user = null;
            string username = string.Empty, password = string.Empty;
            if (Request.Cookies["username"] != null)
                username = Request.Cookies["username"].Value;
            if (Request.Cookies["password"] != null)
                password = Request.Cookies["password"].Value;
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                user = new LoginViewModel { Username = username, Password = password };

            return user;

        }

        public ActionResult Logout()
        {
            //Remove.Sesion
            Session.Remove("username");

            // Remove.Cookie
            if (Response.Cookies["username"] != null)
            {
                HttpCookie ckUsername = new HttpCookie("username");
                ckUsername.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(ckUsername);
            }

            if (Response.Cookies["password"] != null)
            {
                HttpCookie ckPassword = new HttpCookie("password");
                ckPassword.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(ckPassword);
            }

            return RedirectToAction("Login", "Account");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAccount(AccountViewModel model, HttpPostedFileBase upload)
        {
            string fileName = null;
            if (upload != null)
            {
                
                fileName = System.IO.Path.GetFileName(upload.FileName);
               
                upload.SaveAs(Server.MapPath("~/Files/" + fileName));
            }

            model.Picture_Link = fileName;

            if (ModelState.IsValid)
            {

             var user = Mapper.ToRegisterViewModel(model);
                repository.Insert(user);
                repository.Save();
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return RedirectToAction("CreateAccount", "Account");
            }

        }

        public ActionResult CreateAccount()
        {
            AccountViewModel model = new AccountViewModel();
            model.MenuLevel1 = organizerEntities.Categories.Where(menu => menu.ParentId == null)
            .Select(menu => new SelectListItem
            {
                Value = menu.CategoryId.ToString(),
                Text = menu.Name
            }).ToList();

            model.MenuLevel3 = organizerEntities.Skills.Select(menu => new SelectListItem
            {
                Value = menu.SkillId.ToString(),
                Text = menu.Name
            }).ToList();

            model.MenuLevel2 = new List<SelectListItem>();

            return View(model);
        }

        public ActionResult FilterCatLevel2(int id)
        {
            return Json(organizerEntities.Categories
                .Where(c => c.ParentId == id)
                .ToList().Select(c => new SelectListItem

                {
                    Value = c.CategoryId.ToString(),
                    Text = c.Name
                }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PartialLoginBlock()
        {
            return PartialView("_PartialLoginBlock", new AccountViewModel());
        }
    }
}