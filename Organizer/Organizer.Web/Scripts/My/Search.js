﻿
        $(document).ready(function () {
            $("#search").autocomplete({
            source: function (request, response) {
            $.ajax({
                url: "/Home/AutocompleteSearch",
            type: "POST", dataType: "json",
            data: { term: request.term },
            success: function (data) {
            response($.map(data, function (item) {
                return { label: item.value };
            }));
            }
            });
            },
            messages: { noResults: "", results: "" }
            });

     })
    