﻿
$(document).ready(function () {

    $("#pak").hide();


    $("#menuLevel1").kendoDropDownList();

    $("#menuLevel2").kendoDropDownList({
        dataTextField: "Text",
        dataValueField: "Value",
    });

    $("#menuLevel1").change(function () {
        var catId = $('#menuLevel1').val();

        if (catId != 1) {
            $("#pak").show();
        }
        else {
            $("#pak").hide();
        }

        $.ajax({
            type: 'POST',
            data: { id: catId },
            url: '/Account/FilterCatLevel2',
            success: function (result) {

                var ds = new kendo.data.DataSource({ data: result });

                var ddl = $("#menuLevel2").data("kendoDropDownList");
                ddl.setDataSource(ds);

                ddl.value(result[0].Value);

            }

        });

    });


    $("#menuLevel3").kendoDropDownList();
    $("#menuLevel3").change(function () {
        var catId = $("#menuLevel3 option:selected").text();

        var skill = $('#Skills').val();
        if (skill == 0) {
            $('#Skills').val(skill + catId);
        }
        else {
            $('#Skills').val(skill + ", " + catId);
        }

    })

});