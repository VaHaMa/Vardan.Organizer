﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace Organizer.Web.Filters
{
    public class MySpecAuthAttribute: FilterAttribute, IAuthenticationFilter
    {

        
        public void OnAuthentication(AuthenticationContext filterContext)
        {
           // var user = filterContext.HttpContext.Session["username"];

            var categoryId = Convert.ToInt32(filterContext.HttpContext.Session["CategoryId"]);
            if (categoryId ==1)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            var categoryId = Convert.ToInt32(filterContext.HttpContext.Session["CategoryId"]);
            if (categoryId == 1)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new System.Web.Routing.RouteValueDictionary {
                    { "controller", "Account" }, { "action", "Login" }
                   });
            }
        }
    }
}