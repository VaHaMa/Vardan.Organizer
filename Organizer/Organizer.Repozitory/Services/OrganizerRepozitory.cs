﻿using Organizer.Repozitory.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Organizer.Repozitory.Services
{
    public class OrganizerRepository : IOrganizerRepository
    {
        protected OrganizerEntities context;

        public OrganizerRepository()
        {
            context = new OrganizerEntities();
        }

        public void Insert<TModel>(TModel model) where TModel : class
        {
            context.Set<TModel>().Add(model);
        }

        public IEnumerable<TModel> SelectAll<TModel>() where TModel : class
        {
            return context.Set<TModel>().ToList();
        }

        public TModel Get<TModel>(int id) where TModel : class
        {

            return context.Set<TModel>().Find(id);
          
        }

        public void Update<TModel>(TModel model) where TModel : class
        {

            context.Entry(model).State = EntityState.Modified; 
        }


        public void Save()
        {
            context.SaveChanges();
        }


        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                    context = null;
                }
            }
        }


        #endregion
    }
}
