﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Organizer.Repozitory.Services
{
    public interface IOrganizerRepository : IDisposable
    {
        IEnumerable<TModel> SelectAll<TModel>() where TModel : class;

        void Insert<TModel>(TModel model) where TModel : class;

        TModel Get<TModel>(int id) where TModel : class;

        void Update<TModel>(TModel model) where TModel:class;

        void Save();
    }
}
