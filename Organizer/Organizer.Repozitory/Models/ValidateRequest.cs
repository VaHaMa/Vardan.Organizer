﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Organizer.Repozitory.Entities
{
    public class ValidateRequest
    {
        [Display(Name ="Request Date/Time")]
        public DateTime Date_Time { get; set; }

        [Display(Name ="Request Discription")]
        public string Discription { get; set; }


        [Display(Name ="Name / Surname")]
        public string ClientUserId { get; set; }

        [Display(Name = "Please write a reply message")]
        public string Reject_Description { get; set; }


    }
}
