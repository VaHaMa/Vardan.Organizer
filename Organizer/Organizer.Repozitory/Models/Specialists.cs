﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Organizer.Repozitory.Entities
{
    [MetadataType(typeof(ValidateSpecialist))]
    public partial class User
    {
        [Display(Name = "Name / Surname")]
        public string FullName
        {
            get
            {
                return Name + " " + Surname;
            }
        }


        public int Rate { get; set; }

        public int Popular { get; set; }
    }
}
